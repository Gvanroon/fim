# fim

**find + vim = fim**


## INSTALL INSTRUCTIONS
1)  Add "add_my_content_to_bashrc" contents to .bashrc or .bash_aliases
2)  Change "WORKDIR" path to your prefered directory.
3)  Logout and log back in.

## USAGE INSTRUCTIONS
```shell
fim I_am_a_file_or_whatever
```
or partially


```shell
fim I_am_a_fi
```

In case a list is generated, simply input the corresponding **number** + "< enter >".